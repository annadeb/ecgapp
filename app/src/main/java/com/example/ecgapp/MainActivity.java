package com.example.ecgapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.RecordSample;
import com.google.android.material.navigation.NavigationView;
import com.opencsv.CSVWriter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
    SQLiteEcgHelper sqlHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sqlHelper = new SQLiteEcgHelper(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_people, R.id.nav_records, R.id.nav_ecg)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_export_records:
                List<Long> ids = sqlHelper.getRecordIds();
                SaveAndExportAllRecords(ids);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void SaveAndExportAllRecords(List<Long> ids) {

        String[] fileNames = new String[ids.size()];
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
        } else {
            String folder_main = "ECGdata";
            String fileName;

            for(int i=0;i<ids.size();i++) {

                List<RecordSample> recordsamples = sqlHelper.GetRecordSamplesForRecord(ids.get(i));
                String[] stringValues = new String[recordsamples.size()];
                String[] stringTimestamp = new String[recordsamples.size()];
                for(int j=0; j<stringValues.length; j++)
                {
                    stringValues[j] = Double.toString(recordsamples.get(j).getValue());
                    stringTimestamp[j] =  Long.toString(recordsamples.get(j).getTimestamp());
                }

                File fi = new File(Environment.getExternalStorageDirectory(), folder_main);
                if (!fi.exists()) {
                    fi.mkdirs();
                }

                fileName = "Record_id_" + ids.get(i) + ".csv";
                String filePath = baseDir + File.separator + folder_main + File.separator + fileName;
                fileNames[i] = filePath;
                File f = new File(filePath);
                CSVWriter writer;

                try {

                    if (f.exists() && !f.isDirectory()) {
                        FileWriter mFileWriter = new FileWriter(filePath, true);
                        writer = new CSVWriter(mFileWriter);
                    } else {
                        writer = new CSVWriter(new FileWriter(filePath));
                        writer.writeNext(stringValues);
                        writer.writeNext(stringTimestamp);
                    }
                    writer.flush();
                    writer.close();

                } catch (Exception e) {
                    Log.i("File exception", e.getMessage());
                }
            }
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            String formattedDate = df.format(c);
            String zipFileName = formattedDate+".zip";
            String zipFilePath = baseDir + File.separator + folder_main + File.separator + zipFileName;
            zip(fileNames,zipFilePath);

            Uri fileUri = Uri.parse("file://"+zipFilePath);
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("application/zip");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {""});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.ecg_data) + zipFileName);
            emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.ecg_in_att));
            emailIntent.putExtra(Intent.EXTRA_STREAM,fileUri);
            Intent shareIntent = Intent.createChooser(emailIntent, getString(R.string.share_via));
            startActivity(shareIntent);
        }
    }
    private static final int BUFFER = 80000;

    public void zip(String[] _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.length; i++) {
                Log.v("Compress", "Adding: " + _files[i]);
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
