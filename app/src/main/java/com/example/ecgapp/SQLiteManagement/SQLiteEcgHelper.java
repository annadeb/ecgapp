package com.example.ecgapp.SQLiteManagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.ecgapp.models.Person;
import com.example.ecgapp.models.Record;
import com.example.ecgapp.models.RecordSample;

import java.util.LinkedList;
import java.util.List;

public class SQLiteEcgHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "ECGdatabase.db";
    private static final String DB_PEOPLE_TABLE = "PersonEntities";
    private static final String DB_RECORD_TABLE = "RecordEntities";
    private static final String DB_RECORD_SAMPLE_TABLE = "RecordSampleEntities";
    private static final String DEBUG_TAG = "SqLiteECGManager";


    public static final String PERSON_KEY_ID = "person_id";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String KEY_NAME = "name";
    public static final String NAME_OPTIONS = "TEXT NOT NULL";
    public static final int NAME_COLUMN = 1;
    public static final String KEY_SURNAME = "surname";
    public static final String SURNAME_OPTIONS = "TEXT NOT NULL";
    public static final int SURNAME_COLUMN = 2;


    public static final String RECORD_KEY_ID = "record_id";
    public static final String ID_RECORD_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String ID_PERSON_OPTIONS = "INTEGER NOT NULL";
    public static final String KEY_DATE = "date";
    public static final String KEY_DATE_OPTIONS = "TEXT NOT NULL";


    public static final String RECORD_SAMPLE_KEY_ID = "recordsample_id";
    public static final String ID_RECORD_SAMPLE_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String RECORD_KEY_ID_SAMP = "record_id";
    public static final String ID_RECORD_SAM_OPTIONS = "INTEGER NOT NULL";
    public static final String KEY_VALUE = "value";
    public static final String KEY_VALUE_OPTIONS = "REAL NOT NULL";
    public static final String ON_DELETE = "ON DELETE CASCADE";
    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String KEY_TIMESTAMP_OPTIONS = "INTEGER NOT NULL";




    private static final String DB_CREATE_PEOPLE_TABLE =
            "CREATE TABLE " + DB_PEOPLE_TABLE + "( " +
                    PERSON_KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_NAME + " " + NAME_OPTIONS + ", " +
                    KEY_SURNAME + " " + SURNAME_OPTIONS +
                    ");";

    private static final String DB_CREATE_RECORD_TABLE =
            "CREATE TABLE " + DB_RECORD_TABLE + "( " +
                    RECORD_KEY_ID + " " + ID_RECORD_OPTIONS + ", " +
                    PERSON_KEY_ID + " " + ID_PERSON_OPTIONS +  ", " +
                    KEY_DATE + " " + KEY_DATE_OPTIONS +  ", " +
                    "FOREIGN KEY(" + PERSON_KEY_ID + ") REFERENCES "+ DB_PEOPLE_TABLE+ "(" + PERSON_KEY_ID + ") " + ON_DELETE +
                    ");";

    private static final String DB_CREATE_RECORD_SAMPLE_TABLE =
            "CREATE TABLE " + DB_RECORD_SAMPLE_TABLE + "( " +
                    RECORD_SAMPLE_KEY_ID + " " + ID_RECORD_SAMPLE_OPTIONS + ", " +
                    RECORD_KEY_ID_SAMP + " " + ID_RECORD_SAM_OPTIONS + ", " +
                    KEY_VALUE + " " + KEY_VALUE_OPTIONS + ", " +
                    KEY_TIMESTAMP + " " + KEY_TIMESTAMP_OPTIONS +  ", " +
                    "FOREIGN KEY(" + RECORD_KEY_ID_SAMP + ") REFERENCES "+ DB_RECORD_TABLE+ "(" + RECORD_KEY_ID_SAMP + ") " + ON_DELETE +
                    ");";

    private static final String DROP_PEOPLE_TABLE =
            "DROP TABLE IF EXISTS " + DB_PEOPLE_TABLE;

    public SQLiteEcgHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE_PEOPLE_TABLE);

        Log.d(DEBUG_TAG, "Database creating...");
        Log.d(DEBUG_TAG, "Table " + DB_PEOPLE_TABLE + " ver." + DB_VERSION + " created");

        db.execSQL(DB_CREATE_RECORD_TABLE);

        Log.d(DEBUG_TAG, "Table " + DB_RECORD_TABLE + " ver." + DB_VERSION + " created");

        db.execSQL(DB_CREATE_RECORD_SAMPLE_TABLE);

        Log.d(DEBUG_TAG, "Table " + DB_RECORD_SAMPLE_TABLE + " ver." + DB_VERSION + " created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void AddPerson(Person person)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, person.getName());
        cv.put(KEY_SURNAME, person.getSurname());
        db.insertOrThrow(DB_PEOPLE_TABLE, null, cv);
    }

    public void ChangePersonData(Person person)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, person.getName());
        cv.put(KEY_SURNAME, person.getSurname());
        String[] args = {person.getPerson_id() + ""};
        db.update(DB_PEOPLE_TABLE, cv, PERSON_KEY_ID+"=?",args );
    }

    public List<Person> GetAllPeople()
    {
        List<Person> people = new LinkedList<Person>();
        String[] columns = {PERSON_KEY_ID,KEY_NAME,KEY_SURNAME};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_PEOPLE_TABLE,columns,null,null,null,null,null);
        while (cursor.moveToNext()) {
            Person person = new Person();
            person.setPerson_id(cursor.getLong(0));
            person.setName(cursor.getString(1));
            person.setSurname(cursor.getString(2));
            people.add(person);
        }
        return people;
    }

    public void DeletePerson(Long id){
        SQLiteDatabase db = getWritableDatabase();
        String[] args = {"" +id};
        db.delete(DB_PEOPLE_TABLE, PERSON_KEY_ID+"=?",args );
    }

    public Person GetPerson(Long id)
    {
        Person person = new Person();
        String[] columns = {"person_id","name","surname"};
        SQLiteDatabase db = getReadableDatabase();
        String[] args = {id + ""};
        Cursor cursor = db.query(DB_PEOPLE_TABLE,columns,PERSON_KEY_ID+"=?",args,null,null,null,null);
        if(cursor!=null){
            cursor.moveToFirst();
            person.setPerson_id(cursor.getLong(0));
            person.setName(cursor.getString(1));
            person.setSurname(cursor.getString(2));
        }

        return person;
    }

    public void AddRecord(Record record){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(PERSON_KEY_ID, record.getPerson_id());
        cv.put(KEY_DATE, record.getDate());
        db.insertOrThrow(DB_RECORD_TABLE, null, cv);
    }

    public List<Record> GetAllRecords()
    {
        List<Record> records = new LinkedList<Record>();
        String[] columns = {RECORD_KEY_ID,PERSON_KEY_ID,KEY_DATE};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_RECORD_TABLE,columns,null,null,null,null,null);
        while (cursor.moveToNext()) {
            Record record = new Record();
            record.setRecord_id(cursor.getLong(0));
            record.setPerson_id(cursor.getLong(1));
            record.setDate(cursor.getString(2));
            records.add(record);
        }
        return records;
    }


    public void DeleteRecordsForPerson(long id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DB_RECORD_TABLE,PERSON_KEY_ID  + "=" + id,null);
    }


    public void AddRecordSample(RecordSample sample) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(RECORD_KEY_ID_SAMP, sample.getRecord_id());
        cv.put(KEY_VALUE, sample.getValue());
        cv.put(KEY_TIMESTAMP, sample.getTimestamp());
        db.insertOrThrow(DB_RECORD_SAMPLE_TABLE, null, cv);

    }

    public List<RecordSample> GetAllRecordSamples()
    {
        List<RecordSample> records = new LinkedList<RecordSample>();
        String[] columns = {RECORD_SAMPLE_KEY_ID,RECORD_KEY_ID_SAMP,KEY_VALUE};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_RECORD_SAMPLE_TABLE,columns,null,null,null,null,null);
        while (cursor.moveToNext()) {
            RecordSample record = new RecordSample();
            record.setRecordSample_id(cursor.getLong(0));
            record.setRecord_id(cursor.getLong(1));
            record.setValue(cursor.getDouble(2));
            records.add(record);
        }
        return records;
    }

    public List<RecordSample> GetRecordSamplesForRecord(Long id)
    {
        List<RecordSample> records = new LinkedList<RecordSample>();
        String[] columns = {RECORD_SAMPLE_KEY_ID,RECORD_KEY_ID_SAMP,KEY_VALUE,KEY_TIMESTAMP};
        SQLiteDatabase db = getReadableDatabase();
        String[] args = {id + ""};
        Cursor cursor = db.query(DB_RECORD_SAMPLE_TABLE,columns,RECORD_KEY_ID_SAMP+"=?",args,null,null,null);
        while (cursor.moveToNext()) {
            RecordSample record = new RecordSample();
            record.setRecordSample_id(cursor.getLong(0));
            record.setRecord_id(cursor.getLong(1));
            record.setValue(cursor.getDouble(2));
            record.setTimestamp(cursor.getLong(3));
            records.add(record);
        }
        return records;
    }
    public List<Long> getRecordIds() {
        List<Long> recordIds = new LinkedList<Long>();
        String[] columns = {RECORD_KEY_ID};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_RECORD_TABLE,columns,null,null,null,null,null);
        while (cursor.moveToNext()) {
            recordIds.add(cursor.getLong(0));
        }
        return recordIds;
    }

}
