package com.example.ecgapp.models;

public class RecordSample {
    private Long recordSample_id;
    private Long record_id;
    private double value;

    private Long timestamp;

    public Long getRecordSample_id() {
        return recordSample_id;
    }

    public void setRecordSample_id(Long recordSample_id) {
        this.recordSample_id = recordSample_id;
    }

    public Long getRecord_id() {
        return record_id;
    }

    public void setRecord_id(Long record_id) {
        this.record_id = record_id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
