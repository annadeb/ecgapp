package com.example.ecgapp.ui.addECG;

import android.content.Context;
import android.graphics.Paint;

import com.androidplot.xy.AdvancedLineAndPointRenderer;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYSeries;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DynamicPlotter {
    String title;
    private String TAG = "Polar_Plotter";
    private DynamicListener listener;
    private Context context;
    private FadeFormatter formatter;
    private XYSeries series;
    private Queue plotQueue;


    public DynamicPlotter(Context context, String title){
        this.context = context;
        this.title = title;
        plotQueue = new LinkedList();

        for(int i = 0; i < 50; i++){
            plotQueue.add(0);
        }

        formatter = new FadeFormatter(800);
        formatter.setLegendIconEnabled(false);
        List listSeries = (List)plotQueue;
        series = new SimpleXYSeries(listSeries, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, title);
    }

    public SimpleXYSeries getSeries(){
        return (SimpleXYSeries) series;
    }

    public FadeFormatter getFormatter(){
        return formatter;
    }
    private int i = 0;
    public void sendSingleSample(float mV){

        while (i<250)
        {
            mV = 0f;
            break;
        }
        i++;
        plotQueue.add(mV);


        if(plotQueue.size() >= 500){
            plotQueue.remove();
        }

        List listSeries = (List)plotQueue;
        ((SimpleXYSeries) series).setModel(listSeries, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
        listener.update();
    }

    public void setListener(DynamicListener listener){
        this.listener = listener;
    }


    public static class FadeFormatter extends AdvancedLineAndPointRenderer.Formatter {
        private int trailSize;

        public FadeFormatter(int trailSize) {
            this.trailSize = trailSize;
        }

        @Override
        public Paint getLinePaint(int thisIndex, int latestIndex, int seriesSize) {
            int offset;
            if(thisIndex > latestIndex) {
                offset = latestIndex + (seriesSize - thisIndex);
            } else {
                offset =  latestIndex - thisIndex;
            }
            float scale = 255f / trailSize;
            int alpha = (int) (255 - (offset * scale));
            getLinePaint().setAlpha(alpha > 0 ? alpha : 0);
            return getLinePaint();
        }
    }
}
