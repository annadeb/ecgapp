package com.example.ecgapp.ui.addECG;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.FastLineAndPointRenderer;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.example.ecgapp.R;
import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.Person;
import com.example.ecgapp.models.Record;
import com.example.ecgapp.models.RecordSample;

import org.reactivestreams.Publisher;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import polar.com.sdk.api.PolarBleApi;
import polar.com.sdk.api.PolarBleApiCallback;
import polar.com.sdk.api.PolarBleApiDefaultImpl;
import polar.com.sdk.api.errors.PolarInvalidArgument;
import polar.com.sdk.api.model.PolarDeviceInfo;
import polar.com.sdk.api.model.PolarEcgData;
import polar.com.sdk.api.model.PolarHrData;
import polar.com.sdk.api.model.PolarSensorSetting;

public class ECGFragment extends Fragment implements DynamicListener{

    Button buttonStart, buttonStop, buttonChangeID;
    TextView tvDviceID;
    private Spinner spinner;
    String[] peopleStrings;
    private Long personID ;
    private SQLiteEcgHelper sqlHelper;
    private List<Person> people;
    private XYPlot plot;
    private XYSeries series;
    private Integer HISTORY_SIZE = 500;
    private DynamicPlotter dplotter;
    private Disposable ecgDisposable = null;
    private String TAG = "Polar_ECGActivity";
    private String DEVICE_ID;
    private Long recordID;
    PolarBleApi api;
    DynamicListener dl;
    SharedPreferences sharedPreferences;
    private String sharedPrefsKey = "polar_device_id";
    private Boolean connected = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_ecg, container, false);
        final TextView textView = root.findViewById(R.id.text_sel_person);
        buttonStart = root.findViewById(R.id.buttonStart);
        buttonStop = root.findViewById(R.id.buttonStop);
        plot = root.findViewById(R.id.ECGDynamicPlot);
        buttonChangeID = root.findViewById(R.id.buttonChangeID);
        tvDviceID = root.findViewById((R.id.text_device_id));
        DEVICE_ID = sharedPreferences.getString(sharedPrefsKey,"");

        if(DEVICE_ID.equals("")) {
            String textIdNone = getResources().getText(R.string.device_id).toString() + getResources().getText(R.string.none);
            tvDviceID.setText(textIdNone);
        }
        else {
            tvDviceID.setText(getResources().getText(R.string.device_id) + DEVICE_ID);
        }
        spinner = root.findViewById(R.id.spinner_person);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item,peopleStrings);



        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int i=0;
                for(Person per:people)
                {
                    String perString = per.getName() + " " + per.getSurname();
                    if (peopleStrings[position].contains(perString)){
                        personID=per.getPerson_id();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                personID=1L;
            }
        });

        dplotter.setListener(dl);

        series = dplotter.getSeries();

        plot.addSeries(series,
                new FastLineAndPointRenderer.Formatter( getResources().getColor(R.color.white), null, null));

        plot.setRangeBoundaries(-3.0, 3.0, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 0.5);
        plot.setDomainBoundaries(0,    HISTORY_SIZE, BoundaryMode.FIXED);
        plot.setDomainStep(StepMode.INCREMENT_BY_FIT,130);

        plot.setLinesPerRangeLabel(3);
        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#.#"));


        buttonChangeID.setOnClickListener(view -> showDialog(view));
        buttonStart.setOnClickListener(view -> {
            if(DEVICE_ID.equals("")){
                showDialog(view);
            }
            else {
                String waitForDevice = getResources().getText(R.string.wait) + DEVICE_ID;
                Toast.makeText(getContext(), waitForDevice, Toast.LENGTH_LONG).show();

                api = PolarBleApiDefaultImpl.defaultImplementation(getContext(),
                        PolarBleApi.FEATURE_POLAR_SENSOR_STREAMING |
                                PolarBleApi.FEATURE_BATTERY_INFO |
                                PolarBleApi.FEATURE_DEVICE_INFO |
                                PolarBleApi.FEATURE_HR);

                api.setApiCallback(new PolarBleApiCallback() {
                    @Override
                    public void blePowerStateChanged(boolean b) {
                        Log.d(TAG, "BluetoothStateChanged " + b);
                    }

                    @Override
                    public void deviceConnected(PolarDeviceInfo s) {
                        Log.d(TAG, "Device connected " + s.deviceId);
                        connected = true;
                        Toast.makeText(getContext(), R.string.connected, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void deviceConnecting(PolarDeviceInfo polarDeviceInfo) {

                    }

                    @Override
                    public void deviceDisconnected(PolarDeviceInfo s) {
                        Log.d(TAG, "Device disconnected " + s);
                        connected = false;
                        Toast.makeText(getContext(), R.string.device_disconnected, Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void ecgFeatureReady(String s) {
                        Log.d(TAG, "ECG Feature ready " + s);
                        //sqlHelper = new SQLiteEcgHelper(context);
                        Toast.makeText(getContext(), R.string.starting, Toast.LENGTH_LONG).show();
                        Record record = new Record();
                        record.setPerson_id(personID);
                        Date c = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_SSS");
                        String formattedDate = df.format(c);
                        record.setDate(formattedDate);
                        sqlHelper.AddRecord(record);

                        List<Long> ids = sqlHelper.getRecordIds();
                        int index = ids.size();
                        recordID = ids.get(index - 1);
                        streamECG();
                    }

                    @Override
                    public void accelerometerFeatureReady(String s) {
                        Log.d(TAG, "ACC Feature ready " + s);
                    }

                    @Override
                    public void ppgFeatureReady(String s) {
                        Log.d(TAG, "PPG Feature ready " + s);
                    }

                    @Override
                    public void ppiFeatureReady(String s) {
                        Log.d(TAG, "PPI Feature ready " + s);
                    }

                    @Override
                    public void biozFeatureReady(String s) {

                    }

                    @Override
                    public void hrFeatureReady(String s) {
                        Log.d(TAG, "HR Feature ready " + s);
                    }

                    @Override
                    public void disInformationReceived(String s, UUID u, String s1) {
                        if (u.equals(UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb"))) {
                            String msg = "Firmware: " + s1.trim();
                            Log.d(TAG, "Firmware: " + s + " " + s1.trim());
                        }
                    }

                    @Override
                    public void batteryLevelReceived(String s, int i) {
                        String msg = "ID: " + s + "\nBattery level: " + i;
                        Log.d(TAG, "Battery level " + s + " " + i);
                    }

                    @Override
                    public void hrNotificationReceived(String s,
                                                       PolarHrData polarHrData) {
                        Log.d(TAG, "HR " + polarHrData.hr);
                    }

                    @Override
                    public void polarFtpFeatureReady(String s) {
                        Log.d(TAG, "Polar FTP ready " + s);
                    }
                });
                try {
                    api.connectToDevice(DEVICE_ID);
                } catch (PolarInvalidArgument a) {
                    a.printStackTrace();
                }

            }

       });

        buttonStop.setOnClickListener(view -> {
            if (api!=null) {
                api.shutDown();
                Toast.makeText(getContext(), R.string.stopped, Toast.LENGTH_LONG).show();
            }
            connected = false;

        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        checkBT();
        sqlHelper = new SQLiteEcgHelper(getActivity());
        people = sqlHelper.GetAllPeople();
        peopleStrings=new String[people.size()];

        int i=0;
        for(Person per:people)
        {
            peopleStrings[i] = per.getName() + " " + per.getSurname();
            i++;
        }

        dplotter = new DynamicPlotter(getContext(), "ECG");
        this.dl = this;
    }

    public void streamECG() {
        if (ecgDisposable == null) {
            ecgDisposable =
                    api.requestEcgSettings(DEVICE_ID).toFlowable().flatMap((Function<PolarSensorSetting, Publisher<PolarEcgData>>) sensorSetting -> api.startEcgStreaming(DEVICE_ID,
                            sensorSetting.maxSettings())).observeOn(Schedulers.io()).subscribe(
                            polarEcgData -> {
                                Log.d(TAG, "ecg update");
                                for (Integer data : polarEcgData.samples) {
                                    long timestamp = polarEcgData.timeStamp;
                                    RecordSample sample = new RecordSample();
                                    sample.setRecord_id(recordID);
                                    float value = (float) ((float) data / 1000.0);
                                    sample.setValue(value);
                                    sample.setTimestamp(timestamp);
                                    sqlHelper.AddRecordSample(sample);
                                    dplotter.sendSingleSample(value);
                                }
                            },
                            throwable -> {
                                Log.e(TAG,
                                        "" + throwable.getLocalizedMessage());
                                ecgDisposable = null;
                            },
                            () -> Log.d(TAG, "complete")
                    );
        } else {
            ecgDisposable.dispose();
            ecgDisposable = null;
        }
    }

    public void checkBT(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 2);
        }

        if(Build.VERSION.SDK_INT >= 23){
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
    }

    private void showDialog(View view) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(getResources().getString(R.string.enterId));
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0,15,0,0);
        title.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        title.setTextSize(18);

        dialog.setCustomTitle(title);
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.people_add,(ViewGroup) view.getRootView() , false);

        final EditText input = viewInflated.findViewById(R.id.input);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(DEVICE_ID);
        dialog.setView(viewInflated);

        dialog.setPositiveButton("OK", (dialog1, which) -> {
            DEVICE_ID = input.getText().toString();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(sharedPrefsKey, DEVICE_ID);
            editor.apply();
            getFragmentManager().beginTransaction().replace(getParentFragment().getId(),new ECGFragment(),"fragment_ecg_tag").commit();

        });

        dialog.setNegativeButton(getResources().getString(R.string.cancel), (dialog12, which) -> dialog12.cancel());

        dialog.show();
    }
    @Override
    public void update() {

        getActivity().runOnUiThread(() -> {
            if (connected) {
                plot.redraw();
            }
        });
    }
}