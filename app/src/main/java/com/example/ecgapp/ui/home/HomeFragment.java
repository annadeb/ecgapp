package com.example.ecgapp.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ecgapp.R;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment {



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        for (Fragment fragment : getFragmentManager().getFragments()) {
            if (fragment instanceof HomeFragment) {
                continue;
            } else if (fragment != null) {
                getFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }
}