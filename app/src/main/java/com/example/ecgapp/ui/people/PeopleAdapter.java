package com.example.ecgapp.ui.people;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ecgapp.R;
import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.Person;

import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.PeopleViewHolder> {

    Context context;
    List<Person> list;
    FragmentManager fm;
    int parentFragmentId;

    public PeopleAdapter(Context context, List<Person> list, FragmentManager fm, int parentFragmentId) {
        this.list = list;
        this.context=context;
        this.fm = fm;
        this.parentFragmentId = parentFragmentId;
    }

    public class PeopleViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        Button button;

        public PeopleViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.person_tv);
            button = v.findViewById(R.id.edit_person);
        }
    }

    @Override
    public PeopleAdapter.PeopleViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.people_item, parent, false);

        PeopleViewHolder vh = new PeopleViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(PeopleViewHolder holder, int position) {
        final Long personId = list.get(position).getPerson_id();
        holder.textView.setText((list.get(position).getName()+" "+list.get(position).getSurname()));
        holder.button.setOnClickListener(view -> {
            SQLiteEcgHelper sqlHelper = new SQLiteEcgHelper(context);
            sqlHelper.DeletePerson(personId);
            sqlHelper.DeleteRecordsForPerson(personId);
            fm.beginTransaction().replace(parentFragmentId, new PeopleFragment(), "fragment_people_tag").commit();
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
