package com.example.ecgapp.ui.people;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ecgapp.R;
import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.Person;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PeopleFragment extends Fragment {

    private View mainView;
    private RecyclerView recyclerView;
    private List<Person> people;
    private Button editPerson;
    private SQLiteEcgHelper sqlHelper;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_people, container, false);

        FloatingActionButton fab = mainView.findViewById(R.id.people_fab);
        fab.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);

            TextView title = new TextView(getContext());
            title.setText(getResources().getString(R.string.enterPerson));
            title.setGravity(Gravity.CENTER_HORIZONTAL);
            title.setPadding(0,15,0,0);
            title.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            title.setTextSize(18);

            dialog.setCustomTitle(title);
            View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.people_add,(ViewGroup) view.getRootView() , false);

            final EditText input = viewInflated.findViewById(R.id.input);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            dialog.setView(viewInflated);

            dialog.setPositiveButton(R.string.ok, (dialog1, which) -> {
                Person person = new Person ();
                String data = input.getText().toString();
                String[] dataSplit = data.split(" ");
                if (dataSplit.length==2) {
                    person.setName(dataSplit[0]);
                    person.setSurname(dataSplit[1]);
                    sqlHelper.AddPerson(person);
                    people = sqlHelper.GetAllPeople();
                    getFragmentManager().beginTransaction().replace(getParentFragment().getId(), new PeopleFragment(), "fragment_people_tag").commit();
                }
                else if (dataSplit.length<2){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.dialog_message_insert_name).setTitle(R.string.dialog_Error);
                    builder.setPositiveButton(R.string.ok, (dialog11, id) -> {

                    });
                    AlertDialog errorDialog = builder.create();
                    errorDialog.show();

                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.dialog_message_onlu_one_name).setTitle(R.string.dialog_Error);
                    builder.setPositiveButton(R.string.ok, (dialog112, id) -> {

                    });
                    AlertDialog errorDialog = builder.create();
                    errorDialog.show();
                }
            });

            dialog.setNegativeButton(getResources().getString(R.string.cancel), (dialog12, which) -> dialog12.cancel());

            dialog.show();
        });
        recyclerView = mainView.findViewById(R.id.people_rv);
        PeopleAdapter adapter = new PeopleAdapter(getActivity(), people, getFragmentManager(),getParentFragment().getId());
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        init(mainView);


        return mainView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sqlHelper = new SQLiteEcgHelper(getActivity());
        people = sqlHelper.GetAllPeople();
    }

    public void init(View v) {
        editPerson = v.findViewById(R.id.edit_person);
    }
}