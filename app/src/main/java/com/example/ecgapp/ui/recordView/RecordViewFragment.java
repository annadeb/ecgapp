package com.example.ecgapp.ui.recordView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.DashPathEffect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.FastLineAndPointRenderer;
import com.androidplot.xy.PanZoom;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.example.ecgapp.R;
import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.RecordSample;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

public class RecordViewFragment extends Fragment {

    private View mainView;
    private List<RecordSample> recordSamples;
    private String[] stringValues;
    private List<Double> plotPoints;
    private List<BigDecimal> plotPointsX;
    private String[] stringTimestamp;
    private List<Double> recordValues;
    private Long recordID;
    private TextView tvrecordId;
    private Button shareButton;
    private XYPlot plot;
    private XYSeries series;
    private SQLiteEcgHelper sqlHelper;
    private String date;
    private String name;
    private String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();


    public RecordViewFragment(Long recordID, String date, String name){
        this.recordID=recordID;
        this.date=date;
        this.name=name;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_record_view, container, false);
        tvrecordId = mainView.findViewById(R.id.tvrecordId);
        tvrecordId.setText( getString(R.string.record_number) + recordID.toString()+ getString(R.string.person) +name+"_"+date+getString(R.string.number_of_rs) + recordSamples.size());
        shareButton = mainView.findViewById(R.id.buttonShare);
        plot = mainView.findViewById(R.id.plot);

        shareButton.setOnClickListener(view -> ExportRecordSamples());

        series = new SimpleXYSeries(plotPointsX, plotPoints, "ECG series");
        plot.getGraph().setLineLabelEdges(XYGraphWidget.Edge.LEFT, XYGraphWidget.Edge.BOTTOM);

        plot.addSeries(series,
                new FastLineAndPointRenderer.Formatter( getResources().getColor(R.color.white), null, null));
        plot.setRangeBoundaries(-3, 3, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 0.5);
        plot.setDomainBoundaries(0,    5, BoundaryMode.FIXED);
        plot.setDomainStep(StepMode.INCREMENT_BY_VAL,1);
        plot.setLinesPerRangeLabel(3);
        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#.#"));
        PanZoom.attach(plot, PanZoom.Pan.HORIZONTAL, PanZoom.Zoom.STRETCH_HORIZONTAL);

        DashPathEffect dashFx = new DashPathEffect(
                new float[] {PixelUtils.dpToPix(3), PixelUtils.dpToPix(3)}, 0);
        plot.getGraph().getDomainGridLinePaint().setPathEffect(dashFx);
        plot.getGraph().getRangeGridLinePaint().setPathEffect(dashFx);

        return mainView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sqlHelper = new SQLiteEcgHelper(getActivity());

        recordSamples = sqlHelper.GetRecordSamplesForRecord(recordID);
        recordValues = new LinkedList<>();
        stringValues = new String[recordSamples.size()];
        stringTimestamp = new String[recordSamples.size()];

        plotPoints = new LinkedList<>();
        plotPointsX = new LinkedList<>();
        if(recordSamples.size()>5000)
        {
            for(int i=0; i<recordSamples.size(); i++)
            {
                recordValues.add(recordSamples.get(i).getValue());
                stringValues[i] = Double.toString(recordSamples.get(i).getValue());
                stringTimestamp[i] = Long.toString(recordSamples.get(i).getTimestamp());
                if (i<5000){
                    plotPoints.add(recordValues.get(i));
                    if(i==0){
                        plotPointsX.add( new BigDecimal(0));
                    }
                    else{
                        plotPointsX.add(plotPointsX.get(i-1).add(new BigDecimal(1/130)));
                    }
                }
            }
        }
        else{
            for(int i=0;i<recordSamples.size();i++){
                recordValues.add(recordSamples.get(i).getValue());
                stringValues[i] = Double.toString(recordSamples.get(i).getValue());
                stringTimestamp[i] = Long.toString(recordSamples.get(i).getTimestamp());
                plotPoints.add(recordValues.get(i));
                if(i==0){
                    plotPointsX.add( new BigDecimal("0.00000"));
                }
                else{
                    BigDecimal bd = new BigDecimal("1.00000").divide(new BigDecimal("130.00000"),5);
                    plotPointsX.add(plotPointsX.get(i-1).add(bd));
                }
            }
        }

    }

    public void ExportRecordSamples() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.wait_file),Toast.LENGTH_LONG);
            String folder_main = "ECGdata";

            File fi = new File(Environment.getExternalStorageDirectory(), folder_main);
            if (!fi.exists()) {
                fi.mkdirs();
            }

            String fileName = recordID+"_"+name+"_"+date+".csv";
            String filePath = baseDir + File.separator + folder_main + File.separator + fileName;
            File f = new File(filePath);
            CSVWriter writer;

            try {

                if(f.exists()&&!f.isDirectory())
                {
                    FileWriter mFileWriter = new FileWriter(filePath, true);
                    writer = new CSVWriter(mFileWriter);
                }
                else
                {
                    writer = new CSVWriter(new FileWriter(filePath));
                    writer.writeNext(stringValues);
                    writer.writeNext(stringTimestamp);
                }

                writer.flush();
                writer.close();

                Uri fileUri =Uri.fromFile(f);
                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {""});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.ecg_data) + fileName);
                emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.ecg_in_att));
                emailIntent.putExtra(Intent.EXTRA_STREAM,fileUri);

                Intent shareIntent = Intent.createChooser(emailIntent, getString(R.string.share_via));
                startActivity(shareIntent);

            }
            catch (Exception e) {
                Log.i("File exception",e.getMessage());
            }
        }

    }
}
