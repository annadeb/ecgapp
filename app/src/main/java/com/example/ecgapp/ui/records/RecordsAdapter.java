package com.example.ecgapp.ui.records;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ecgapp.R;
import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.Person;
import com.example.ecgapp.models.Record;
import com.example.ecgapp.ui.recordView.RecordViewFragment;

import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.RecordsViewHolder> {

    Context context;
    List<Record> list;
    FragmentManager fm;
    int parentFragmentId;

    public RecordsAdapter(Context context, List<Record> list, FragmentManager fm, int parentFragmentId) {
        this.list = list;
        this.context=context;
        this.fm = fm;
        this.parentFragmentId = parentFragmentId;
    }
public class RecordsViewHolder extends RecyclerView.ViewHolder {

    public TextView textView;
    Button button;

    public RecordsViewHolder(View v) {
        super(v);
        textView = v.findViewById(R.id.record_tv);
        button = v.findViewById(R.id.view_record);
    }
}

    @Override
    public RecordsAdapter.RecordsViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.records_item, parent, false);

        RecordsViewHolder vh = new RecordsViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecordsViewHolder holder, int position) {

        SQLiteEcgHelper sqlHelper = new SQLiteEcgHelper(context);
        Long personId = list.get(position).getPerson_id();
        Person person = sqlHelper.GetPerson(personId);
        final String date = list.get(position).getDate();
        String formattedDate = date.substring(0,16);
        final Long recordID = list.get(position).getRecord_id();
        final String name = person.getName();
        final String surname = person.getSurname();


        holder.textView.setText((recordID+" - "+name+" " + surname + " - "+ formattedDate ));
        holder.button.setOnClickListener(view -> {
            RecordViewFragment recordViewFragment = new RecordViewFragment(recordID, date, name+"_" + surname );
            fm.beginTransaction().replace(parentFragmentId, recordViewFragment,"fragment_view_tag").commit();

        });

    }
    @Override
    public int getItemCount() {
        return list.size();
    }
}