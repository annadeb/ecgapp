package com.example.ecgapp.ui.records;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ecgapp.R;
import com.example.ecgapp.SQLiteManagement.SQLiteEcgHelper;
import com.example.ecgapp.models.Record;
import com.example.ecgapp.ui.addECG.ECGFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RecordsFragment extends Fragment {

    private View mainView;
    private RecyclerView recyclerView;
    private List<Record> records;
    private Button viewRecord;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_records, container, false);
        FloatingActionButton fab = mainView.findViewById(R.id.records_fab);
        fab.setOnClickListener(view -> {
            getFragmentManager().beginTransaction().replace(getParentFragment().getId(),new ECGFragment(),"fragment_ecg_tag").commit();
        });
        recyclerView = mainView.findViewById(R.id.records_rv);
        RecordsAdapter adapter = new RecordsAdapter(getActivity(), records, getFragmentManager(),getParentFragment().getId());
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        init(mainView);


        return mainView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SQLiteEcgHelper sqlHelper = new SQLiteEcgHelper(getActivity());

        records = sqlHelper.GetAllRecords();
    }

    public void init(View v) {
        viewRecord = v.findViewById(R.id.view_record);
    }
}